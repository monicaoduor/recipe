import React from 'react'
import AddRecipe from './AddRecipeModal'
import {Modal, Button} from 'react-bootstrap'

export default function AddRecipeBtn() {
    const [show, setShow] = React.useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <div>
            <Button className='add-recipe-btn' block size="lg"  onClick={handleShow}>
            Add recipe
            </Button>
                <Modal show={show} onHide={handleClose}>
                  <AddRecipe />
                </Modal>
        </div>
    )
}
