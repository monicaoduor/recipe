import React, { Component } from 'react'

export default class AddRecipe extends Component {
    constructor(props){
        super (props)
        this.state = {
            fname: '',
            time: '',
            ingredients: '',
            servings: ''
        }
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event){
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    }


    render() {
        return (
            <div className='addRecipe'>
                 <form className='form'>
                      <label htmlFor="name">Name:</label><br />
                      <input type="text" onChange={this.handleChange}id="fname" name="fname" placeholder="Chapati" /><br />

                      <label htmlFor="time">Time:</label><br />
                      <input type="text" onChange={this.handleChange} id="time" name="time" placeholder="eg. 1hr to cook}" /><br />

                      <label htmlFor="imgUrl">ImgUrl:</label><br />
                      <textarea type="text" onChange={this.handleChange}  id="imgUrl" name="imgUrl" placeholder="ImgUrl(optional)" /><br />

                      <label htmlFor="ingredients">Ingredients:</label><br />
                      <textarea type="text" onChange={this.handleChange}  id="ingredients" name="ingredients" placeholder="Wheat, water, ..." /><br />

                      <label htmlFor="servings">Servings:</label>
                      <select
                        value={this.state.servings}
                        onChange={this.handleChange}
                        name="servings"
                      >
                          <option value='1'>1</option>
                          <option value='2'>2</option>
                          <option value='3'>3</option>
                          <option value='4'>4</option>
                      </select>
                      <br />

                      <br />
                      <input type="submit" value="Submit" />
                    </form>
                    <div className='newInputtedRecipes'>
                        <p><span style={{color: 'grey'}}>Recipe name:</span> {this.state.fname} </p>
                        <p><span style={{color: 'grey'}}>Time:</span> {this.state.time} </p>
                        <p><span style={{color: 'grey'}}>ImgUrl:</span> {this.state.imgUrl} </p>
                        <p><span style={{color: 'grey'}}>Ingredients: </span>{this.state.ingredients} </p>
                        <p><span style={{color: 'grey'}}>Servings: </span>{this.state.servings} </p>

                    </div>


            </div>
        )
    }
}
