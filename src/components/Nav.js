import React from 'react'
import {Navbar, Nav} from 'react-bootstrap'

export default function Navigation() {
  return (
    <div>
        <Navbar collapseOnSelect expand="md" variant='light'>
          <Navbar.Brand href="#home" className='logo'><i className="fas fa-utensils"></i> Recipe box</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto"></Nav>
            <Nav>
              <Nav.Link style={{cursor:'text'}} href="#deets"className='btn btn-sm'><i className="fas fa-search"></i></Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </div>
  )
}
