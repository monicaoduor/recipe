import React, { Component } from 'react'
import RecipeItem from './RecipeItem'
import RecipeData from './RecipeData'
import AddRecipeBtn from './AddRecipeBtn'

export default class Recipe extends Component {
    constructor(props){
        super(props)
        this.state = {
            recipes: RecipeData
        }

    }


    render() {
        const recipeItems = this.state.recipes.map((item) =>
            <RecipeItem key={item.id} items = {item}/>
        )
        return (
            <div>
                <AddRecipeBtn />
                <ol className='container'>
                    {recipeItems}
                </ol>
            </div>
        )
    }
}
