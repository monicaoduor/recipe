import React, { Component } from 'react'
import {Card, Accordion} from 'react-bootstrap'

export default class RecipeItem extends Component {
    render() {
        return (
            <div>
                    <Accordion defaultActiveKey="1" className='accordion'>
                        <Card>
                            <Card.Header>
                                <Accordion.Toggle eventKey="0" className='toggle' as={Card.Header}>
                                    <li>&nbsp;{this.props.items.name}</li>

                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <Card className='card'>
                                            <div>
                                                <Card.Title>Ingredients</Card.Title>
                                                <p className='servings'>SERVINGS: {this.props.items.servings} </p>
                                            </div>
                                            <Card.Body>
                                                <Card.Img variant='top' src={this.props.items.imgUrl} className='card-image'></Card.Img><br />
                                                <Card.Text>
                                                    <span style = {{color: 'grey'}}> {this.props.items.ingredients} <br /> {this.props.items.time} </span>
                                                    <br />
                                                    <button className='btn' onClick={this.updateRecipe}>
                                                        &nbsp;&nbsp;<i className="fas fa-pen"></i> Edit
                                                    </button>
                                                    <button className='btn' onClick={this.handleDelete}>
                                                        &nbsp;&nbsp;<i className="fas fa-trash-alt"></i> Delete
                                                    </button>
                                            </Card.Text>

                                        </Card.Body>
                                    </Card>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>

                    </Accordion>



                </div>
        )
    }
}
