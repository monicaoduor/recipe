const RecipeData = [
    {
        id: 1,
        name: 'Chapati',
        ingredients: ['1/2 cup wheat flour, ', '1/4 teaspoon salt, ', 'water, '],
        imgUrl: 'https://images.unsplash.com/photo-1586524068358-77d2196875e7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        time: '1hr',
        servings: 4
    },
    {
        id: 2,
        name: 'Spaghetti and meat',
        ingredients: ['Lean ground beef, ', 'tomato sauce, ', 'beef broth',],
        imgUrl: 'https://images.unsplash.com/photo-1586819286107-b4fb6cc7049f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        time: '1hr 40min',
        servings: 3
    },
    {
        id: 3,
        name: 'Rice',
        ingredients: ['2 cups of rice, ', '2 teaspoons of oil, ', '4 cups of water, ' ],
        imgUrl: 'https://images.unsplash.com/photo-1516684732162-798a0062be99?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        time: '30 min',
        servings: 3
    },
    {
        id: 4,
        name: 'Waffles',
        ingredients: ['Eggs, ', '2 teaspoons baking powder, ', '2 cups all purpose flour, ', '2 tablespoons sugar, ', 'vanilla extract ', '1 teaspoon salt ', '2 eggs ', '2 cups milk, ', '1/3 cups butter'],
        imgUrl: 'https://images.unsplash.com/photo-1562376552-0d160a2f238d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
        time: '25 min',
        servings: 5
    }

]
export default RecipeData;