import React from 'react';
import './App.css';
import ResponsiveDrawer from './components/Nav'
import Recipe from './components/Recipe'

function App() {
  return (
    <div>
      <ResponsiveDrawer />
      <Recipe />
    </div>
  );
}

export default App;
